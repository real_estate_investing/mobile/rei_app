// Generated code, do not modify. Run `build_runner build` to re-generate!
// @dart=2.12
import 'package:web3dart/web3dart.dart' as _i1;

final _contractAbi = _i1.ContractAbi.fromJson(
    '[{"inputs":[],"name":"InsuficientFundsToRetrieve","type":"error"},{"inputs":[],"name":"InvalidAmountOfEther","type":"error"},{"inputs":[],"name":"NotOnWhiteList","type":"error"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"from","type":"address"},{"indexed":false,"internalType":"address","name":"to","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"price","type":"uint256"}],"name":"CloseDeposit","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"securityAddress","type":"address"},{"indexed":false,"internalType":"address","name":"from","type":"address"},{"indexed":false,"internalType":"uint256","name":"amount","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"pricePerToken","type":"uint256"},{"indexed":false,"internalType":"uint256","name":"depositNumber","type":"uint256"}],"name":"CreateDeposit","type":"event"},{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"","type":"address"},{"indexed":false,"internalType":"uint256","name":"","type":"uint256"}],"name":"RetrievePayments","type":"event"},{"inputs":[{"internalType":"address","name":"_secAddress","type":"address"},{"internalType":"uint256","name":"_amount","type":"uint256"},{"internalType":"uint256","name":"_pricePerToken","type":"uint256"}],"name":"addDeposit","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"uint256","name":"_depositNumber","type":"uint256"}],"name":"closeDeposit","outputs":[],"stateMutability":"payable","type":"function"},{"inputs":[],"name":"depositCounter","outputs":[{"internalType":"uint256","name":"_value","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"deposits","outputs":[{"internalType":"address","name":"contractAddress","type":"address"},{"internalType":"address","name":"sellerAddress","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"},{"internalType":"uint256","name":"pricePerToken","type":"uint256"},{"internalType":"bool","name":"closed","type":"bool"},{"internalType":"uint256","name":"depositNumber","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getAllDeposits","outputs":[{"components":[{"internalType":"address","name":"contractAddress","type":"address"},{"internalType":"address","name":"sellerAddress","type":"address"},{"internalType":"uint256","name":"amount","type":"uint256"},{"internalType":"uint256","name":"pricePerToken","type":"uint256"},{"internalType":"bool","name":"closed","type":"bool"},{"internalType":"uint256","name":"depositNumber","type":"uint256"}],"internalType":"struct Deposit[]","name":"","type":"tuple[]"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"getFunds","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"","type":"address"}],"name":"payments","outputs":[{"internalType":"uint256","name":"","type":"uint256"}],"stateMutability":"view","type":"function"}]',
    'TradeContract');

class TradeContract extends _i1.GeneratedContract {
  TradeContract(
      {required _i1.EthereumAddress address,
      required _i1.Web3Client client,
      int? chainId})
      : super(_i1.DeployedContract(_contractAbi, address), client, chainId);

  /// The optional [transaction] parameter can be used to override parameters
  /// like the gas price, nonce and max gas. The `data` and `to` fields will be
  /// set by the contract.
  Future<String> addDeposit(
      _i1.EthereumAddress _secAddress, BigInt _amount, BigInt _pricePerToken,
      {required _i1.Credentials credentials,
      _i1.Transaction? transaction}) async {
    final function = self.abi.functions[0];
    assert(checkSignature(function, 'c08e551b'));
    final params = [_secAddress, _amount, _pricePerToken];
    return write(credentials, transaction, function, params);
  }

  /// The optional [transaction] parameter can be used to override parameters
  /// like the gas price, nonce and max gas. The `data` and `to` fields will be
  /// set by the contract.
  Future<String> closeDeposit(BigInt _depositNumber,
      {required _i1.Credentials credentials,
      _i1.Transaction? transaction}) async {
    final function = self.abi.functions[1];
    assert(checkSignature(function, '83b24c52'));
    final params = [_depositNumber];
    return write(credentials, transaction, function, params);
  }

  /// The optional [atBlock] parameter can be used to view historical data. When
  /// set, the function will be evaluated in the specified block. By default, the
  /// latest on-chain block will be used.
  Future<BigInt> depositCounter({_i1.BlockNum? atBlock}) async {
    final function = self.abi.functions[2];
    assert(checkSignature(function, 'ecb3dc88'));
    final params = [];
    final response = await read(function, params, atBlock);
    return (response[0] as BigInt);
  }

  /// The optional [atBlock] parameter can be used to view historical data. When
  /// set, the function will be evaluated in the specified block. By default, the
  /// latest on-chain block will be used.
  Future<Deposits> deposits(BigInt $param4, {_i1.BlockNum? atBlock}) async {
    final function = self.abi.functions[3];
    assert(checkSignature(function, 'b02c43d0'));
    final params = [$param4];
    final response = await read(function, params, atBlock);
    return Deposits(response);
  }

  /// The optional [atBlock] parameter can be used to view historical data. When
  /// set, the function will be evaluated in the specified block. By default, the
  /// latest on-chain block will be used.
  Future<List<dynamic>> getAllDeposits({_i1.BlockNum? atBlock}) async {
    final function = self.abi.functions[4];
    assert(checkSignature(function, 'd3f3f5ea'));
    final params = [];
    final response = await read(function, params, atBlock);
    return (response[0] as List<dynamic>).cast<dynamic>();
  }

  /// The optional [transaction] parameter can be used to override parameters
  /// like the gas price, nonce and max gas. The `data` and `to` fields will be
  /// set by the contract.
  Future<String> getFunds(
      {required _i1.Credentials credentials,
      _i1.Transaction? transaction}) async {
    final function = self.abi.functions[5];
    assert(checkSignature(function, '4d9b3735'));
    final params = [];
    return write(credentials, transaction, function, params);
  }

  /// The optional [atBlock] parameter can be used to view historical data. When
  /// set, the function will be evaluated in the specified block. By default, the
  /// latest on-chain block will be used.
  Future<BigInt> payments(_i1.EthereumAddress $param5,
      {_i1.BlockNum? atBlock}) async {
    final function = self.abi.functions[6];
    assert(checkSignature(function, 'e2982c21'));
    final params = [$param5];
    final response = await read(function, params, atBlock);
    return (response[0] as BigInt);
  }

  /// Returns a live stream of all CloseDeposit events emitted by this contract.
  Stream<CloseDeposit> closeDepositEvents(
      {_i1.BlockNum? fromBlock, _i1.BlockNum? toBlock}) {
    final event = self.event('CloseDeposit');
    final filter = _i1.FilterOptions.events(
        contract: self, event: event, fromBlock: fromBlock, toBlock: toBlock);
    return client.events(filter).map((_i1.FilterEvent result) {
      final decoded = event.decodeResults(result.topics!, result.data!);
      return CloseDeposit(decoded);
    });
  }

  /// Returns a live stream of all CreateDeposit events emitted by this contract.
  Stream<CreateDeposit> createDepositEvents(
      {_i1.BlockNum? fromBlock, _i1.BlockNum? toBlock}) {
    final event = self.event('CreateDeposit');
    final filter = _i1.FilterOptions.events(
        contract: self, event: event, fromBlock: fromBlock, toBlock: toBlock);
    return client.events(filter).map((_i1.FilterEvent result) {
      final decoded = event.decodeResults(result.topics!, result.data!);
      return CreateDeposit(decoded);
    });
  }

  /// Returns a live stream of all RetrievePayments events emitted by this contract.
  Stream<RetrievePayments> retrievePaymentsEvents(
      {_i1.BlockNum? fromBlock, _i1.BlockNum? toBlock}) {
    final event = self.event('RetrievePayments');
    final filter = _i1.FilterOptions.events(
        contract: self, event: event, fromBlock: fromBlock, toBlock: toBlock);
    return client.events(filter).map((_i1.FilterEvent result) {
      final decoded = event.decodeResults(result.topics!, result.data!);
      return RetrievePayments(decoded);
    });
  }
}

class Deposits {
  Deposits(List<dynamic> response)
      : contractAddress = (response[0] as _i1.EthereumAddress),
        sellerAddress = (response[1] as _i1.EthereumAddress),
        amount = (response[2] as BigInt),
        pricePerToken = (response[3] as BigInt),
        closed = (response[4] as bool),
        depositNumber = (response[5] as BigInt);

  final _i1.EthereumAddress contractAddress;

  final _i1.EthereumAddress sellerAddress;

  final BigInt amount;

  final BigInt pricePerToken;

  final bool closed;

  final BigInt depositNumber;
}

class CloseDeposit {
  CloseDeposit(List<dynamic> response)
      : from = (response[0] as _i1.EthereumAddress),
        to = (response[1] as _i1.EthereumAddress),
        amount = (response[2] as BigInt),
        price = (response[3] as BigInt);

  final _i1.EthereumAddress from;

  final _i1.EthereumAddress to;

  final BigInt amount;

  final BigInt price;
}

class CreateDeposit {
  CreateDeposit(List<dynamic> response)
      : securityAddress = (response[0] as _i1.EthereumAddress),
        from = (response[1] as _i1.EthereumAddress),
        amount = (response[2] as BigInt),
        pricePerToken = (response[3] as BigInt),
        depositNumber = (response[4] as BigInt);

  final _i1.EthereumAddress securityAddress;

  final _i1.EthereumAddress from;

  final BigInt amount;

  final BigInt pricePerToken;

  final BigInt depositNumber;
}

class RetrievePayments {
  RetrievePayments(List<dynamic> response)
      : var1 = (response[0] as _i1.EthereumAddress),
        var2 = (response[1] as BigInt);

  final _i1.EthereumAddress var1;

  final BigInt var2;
}
