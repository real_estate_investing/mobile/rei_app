// Generated code, do not modify. Run `build_runner build` to re-generate!
// @dart=2.12
import 'package:web3dart/web3dart.dart' as _i1;

final _contractAbi = _i1.ContractAbi.fromJson(
    '[{"anonymous":false,"inputs":[{"indexed":false,"internalType":"address","name":"securityAddress","type":"address"}],"name":"SecurityContractCreated","type":"event"},{"inputs":[{"internalType":"string","name":"_countryFilter","type":"string"}],"name":"applyFilter","outputs":[{"internalType":"address[]","name":"","type":"address[]"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"string","name":"_name","type":"string"},{"internalType":"string","name":"_symbol","type":"string"},{"components":[{"internalType":"string","name":"description","type":"string"},{"internalType":"string","name":"addressOfAsset","type":"string"},{"internalType":"string","name":"country","type":"string"},{"internalType":"string","name":"city","type":"string"},{"internalType":"string","name":"typeOfBuilding","type":"string"},{"internalType":"uint16","name":"area","type":"uint16"},{"internalType":"uint16","name":"nrOfFloors","type":"uint16"},{"internalType":"string","name":"folderCID","type":"string"},{"internalType":"string[]","name":"images","type":"string[]"}],"internalType":"struct SecurityInfo","name":"_info","type":"tuple"}],"name":"createSecurityContract","outputs":[{"internalType":"address","name":"securityContractCreated","type":"address"}],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"getAllSecurityContracts","outputs":[{"internalType":"address[]","name":"","type":"address[]"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"tokenIdCounter","outputs":[{"internalType":"uint256","name":"_value","type":"uint256"}],"stateMutability":"view","type":"function"},{"inputs":[{"internalType":"uint256","name":"","type":"uint256"}],"name":"tokenToContract","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"}]',
    'FactoryContract');

class FactoryContract extends _i1.GeneratedContract {
  FactoryContract(
      {required _i1.EthereumAddress address,
      required _i1.Web3Client client,
      int? chainId})
      : super(_i1.DeployedContract(_contractAbi, address), client, chainId);

  /// The optional [atBlock] parameter can be used to view historical data. When
  /// set, the function will be evaluated in the specified block. By default, the
  /// latest on-chain block will be used.
  Future<List<_i1.EthereumAddress>> applyFilter(String _countryFilter,
      {_i1.BlockNum? atBlock}) async {
    final function = self.abi.functions[0];
    assert(checkSignature(function, '87efb7ca'));
    final params = [_countryFilter];
    final response = await read(function, params, atBlock);
    return (response[0] as List<dynamic>).cast<_i1.EthereumAddress>();
  }

  /// The optional [transaction] parameter can be used to override parameters
  /// like the gas price, nonce and max gas. The `data` and `to` fields will be
  /// set by the contract.
  Future<String> createSecurityContract(
      String _name, String _symbol, dynamic _info,
      {required _i1.Credentials credentials,
      _i1.Transaction? transaction}) async {
    final function = self.abi.functions[1];
    assert(checkSignature(function, 'ddbdb329'));
    final params = [_name, _symbol, _info];
    return write(credentials, transaction, function, params);
  }

  /// The optional [atBlock] parameter can be used to view historical data. When
  /// set, the function will be evaluated in the specified block. By default, the
  /// latest on-chain block will be used.
  Future<List<_i1.EthereumAddress>> getAllSecurityContracts(
      {_i1.BlockNum? atBlock}) async {
    final function = self.abi.functions[2];
    assert(checkSignature(function, 'd1d21b12'));
    final params = [];
    final response = await read(function, params, atBlock);
    return (response[0] as List<dynamic>).cast<_i1.EthereumAddress>();
  }

  /// The optional [atBlock] parameter can be used to view historical data. When
  /// set, the function will be evaluated in the specified block. By default, the
  /// latest on-chain block will be used.
  Future<BigInt> tokenIdCounter({_i1.BlockNum? atBlock}) async {
    final function = self.abi.functions[3];
    assert(checkSignature(function, '98bdf6f5'));
    final params = [];
    final response = await read(function, params, atBlock);
    return (response[0] as BigInt);
  }

  /// The optional [atBlock] parameter can be used to view historical data. When
  /// set, the function will be evaluated in the specified block. By default, the
  /// latest on-chain block will be used.
  Future<_i1.EthereumAddress> tokenToContract(BigInt $param4,
      {_i1.BlockNum? atBlock}) async {
    final function = self.abi.functions[4];
    assert(checkSignature(function, 'db5f73b1'));
    final params = [$param4];
    final response = await read(function, params, atBlock);
    return (response[0] as _i1.EthereumAddress);
  }

  /// Returns a live stream of all SecurityContractCreated events emitted by this contract.
  Stream<SecurityContractCreated> securityContractCreatedEvents(
      {_i1.BlockNum? fromBlock, _i1.BlockNum? toBlock}) {
    final event = self.event('SecurityContractCreated');
    final filter = _i1.FilterOptions.events(
        contract: self, event: event, fromBlock: fromBlock, toBlock: toBlock);
    return client.events(filter).map((_i1.FilterEvent result) {
      final decoded = event.decodeResults(result.topics!, result.data!);
      return SecurityContractCreated(decoded);
    });
  }
}

class SecurityContractCreated {
  SecurityContractCreated(List<dynamic> response)
      : securityAddress = (response[0] as _i1.EthereumAddress);

  final _i1.EthereumAddress securityAddress;
}
