library contract_models;

import 'package:json_annotation/json_annotation.dart';
import 'package:web3dart/web3dart.dart';

part 'index.g.dart';
part 'contract_details.dart';
part 'contract_info.dart';