part of contract_models;

@JsonSerializable()
class ContractDetails {
  ContractDetails(
      {required this.name,
      required this.country,
      required this.address,
      required this.city,
      required this.description,
      required this.area,
      required this.typeOfBuilding,
      required this.folderCID,
      required this.images,
      });

  final String? name;
  final String? country;
  final String? city;
  final String? description;
  final String? address;
  final int? area;
  final String? typeOfBuilding;

  @JsonKey(name: "image")
  final String? folderCID;
  final List<String>? images;

  factory ContractDetails.fromJson(Map<String, dynamic> json) => _$ContractDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$ContractDetailsToJson(this);
}
