import 'package:web3dart/credentials.dart';

class DepositModel {
  DepositModel({
    required this.contractAddress,
    required this.sellerAddress,
    required this.amount,
    required this.pricePerToken,
    required this.closed,
    required this.depositNumber,
  });

  final EthereumAddress contractAddress;
  final EthereumAddress sellerAddress;
  final BigInt amount;
  final BigInt pricePerToken;
  final bool closed;
  final BigInt depositNumber;

  DepositModel.fromArray(List<dynamic> array)
      : contractAddress = array[0],
        sellerAddress = array[1],
        amount = array[2],
        pricePerToken = array[3],
        closed = array[4],
        depositNumber = array[5];
}
