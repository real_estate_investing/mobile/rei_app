part of contract_models;

class ContractInfo {
  ContractInfo({required this.address, required this.details, required this.tokenPrice, required this.availableTokens});

  final EthereumAddress address;
  final ContractDetails details;
  final BigInt availableTokens;
  final BigInt tokenPrice;
}
