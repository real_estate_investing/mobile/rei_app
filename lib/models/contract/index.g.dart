// GENERATED CODE - DO NOT MODIFY BY HAND

part of contract_models;

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ContractDetails _$ContractDetailsFromJson(Map<String, dynamic> json) =>
    ContractDetails(
      name: json['name'] as String?,
      country: json['country'] as String?,
      address: json['address'] as String?,
      city: json['city'] as String?,
      description: json['description'] as String?,
      area: json['area'] as int?,
      typeOfBuilding: json['typeOfBuilding'] as String?,
      folderCID: json['image'] as String?,
      images:
          (json['images'] as List<dynamic>?)?.map((e) => e as String).toList(),
    );

Map<String, dynamic> _$ContractDetailsToJson(ContractDetails instance) =>
    <String, dynamic>{
      'name': instance.name,
      'country': instance.country,
      'city': instance.city,
      'description': instance.description,
      'address': instance.address,
      'area': instance.area,
      'typeOfBuilding': instance.typeOfBuilding,
      'image': instance.folderCID,
      'images': instance.images,
    };
