part of auth_models;

@JsonSerializable()
@HiveType(typeId: 0)
class AppUser extends HiveObject{
  AppUser({this.username, required this.email, required this.uid, required this.favorites});

  @HiveField(0)
  final String uid;
  @HiveField(1)
  final String email;
  @HiveField(2)
  final String? username;
  @HiveField(3)
  final List<String> favorites;


  factory AppUser.fromJson(Map<String, dynamic> json) => _$AppUserFromJson(json);

  Map<String, dynamic> toJson() => _$AppUserToJson(this);
}
