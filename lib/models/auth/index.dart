library auth_models;

import 'package:hive/hive.dart';
import 'package:json_annotation/json_annotation.dart';

part 'index.g.dart';
part 'app_user.dart';