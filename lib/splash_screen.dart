import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rei_app/app_routes.dart';
import 'package:rei_app/provider/smart_contract_token_provider.dart';
import 'package:rei_app/provider/factory_contract_provider.dart';
import 'package:rei_app/provider/trade_contract_provider.dart';
import 'package:rei_app/provider/user_provider.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Future.wait([
      context.read<SmartContractTokenProvider>().init(),
      context.read<FactoryContractProvider>().init(),
      context.read<TradeContractProvider>().init(),
      context.read<TradeContractProvider>().loadAbiCode(),
      context.read<SmartContractTokenProvider>().loadAbiCode(),
      context.read<UserProvider>().loadAppUser(),
      Future.delayed(const Duration(seconds: 3)),
    ]).then((value) {
      if (context.read<UserProvider>().appUser == null){
        Navigator.pushReplacementNamed(context, AppRoutes.login);
      }else {
        Navigator.pushReplacementNamed(context, AppRoutes.metamask);
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
