import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:rei_app/models/auth/index.dart';

class AuthApi {
  final FirebaseAuth _auth = FirebaseAuth.instance;

  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  Future<AppUser> registerEmailAndPassword({required String email, required String password}) async {
    final UserCredential credential = await _auth.createUserWithEmailAndPassword(email: email, password: password);
    final User? user = credential.user;
    if (user?.uid == null) {
      throw UnimplementedError('UID_NULL');
    }
    final AppUser newUser = AppUser(email: email, uid: user!.uid, favorites: []);
    _firestore.doc('users/${user.uid}').set(newUser.toJson());
    return newUser;
  }

  Future<AppUser> singInEmailAndPassword({required String email, required String password}) async {
    final UserCredential credential = await _auth.signInWithEmailAndPassword(email: email, password: password);
    final User? user = credential.user;
    if (user?.uid == null) {
      throw UnimplementedError('UID_NULL');
    }
    final DocumentSnapshot snapshot = await _firestore.doc('/users/${user!.uid}').get();

    return AppUser.fromJson(snapshot.data() as Map<String, dynamic>);
  }

  Future<void> signOut() async {
    await _auth.signOut();
  }

  Future<void> updateFavorite({required String uid, required String address, required bool toAdd}) async {
    FieldValue value;
    if (toAdd) {
      value = FieldValue.arrayUnion(<String>[address]);
    } else {
      value = FieldValue.arrayRemove(<String>[address]);
    }
    await _firestore.doc('users/$uid').update(<String, dynamic>{'favorites': value});
  }
}
