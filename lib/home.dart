import 'dart:developer';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:rei_app/app/homepage.dart';
import 'package:rei_app/provider/smart_contract_token_provider.dart';
import 'package:provider/provider.dart';
import 'package:rei_app/utils/rei_colors.dart';
import 'package:web3dart/credentials.dart';

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Selector<SmartContractTokenProvider, EthereumAddress?>(
      selector: (_, model) => model.userWalletAddress,
      builder: (_, data, ___) {
        if (data == null) {
          return Scaffold(
            backgroundColor: ReiColors.gunmetal,
            body: Padding(
              padding: const EdgeInsets.all(20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  Text(
                    'Please connect your Metamask account',
                    style: TextStyle(color: ReiColors.ghostWhite, fontSize: 20, fontWeight: FontWeight.bold, ),textAlign: TextAlign.center,
                  ),
                  Image.asset('assets/images/MetaMask_Fox.png'),
                  ElevatedButton(
                    onPressed: () async {
                      try{
                      final bool result = await context.read<SmartContractTokenProvider>().connectWallet();

                      }catch (error) {
                        log(error.toString());

                          showDialog(
                              context: context,
                              builder: (context) => const AlertDialog(title: Text('Eroare la conectare')));
                        print(error);
                      }
                      // log(result.toString());
                      // if (!result) {
                      //   showDialog(
                      //       context: context,
                      //       builder: (context) => const AlertDialog(title: Text('Eroare la conectare')));
                      // }
                    },
                    style: const ButtonStyle().copyWith(
                        backgroundColor: MaterialStateProperty.all(ReiColors.ghostWhite),
                        shape: MaterialStateProperty.all(
                          RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
                        ),
                        padding: MaterialStateProperty.all(const EdgeInsets.symmetric(horizontal: 20, vertical: 12))),
                    child: Text(
                      'Connect',
                      style: TextStyle(
                        color: ReiColors.gunmetal,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        }
        return const MyHomePage();
      },
    );
  }
}
