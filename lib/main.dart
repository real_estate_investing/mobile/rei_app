import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:rei_app/app_routes.dart';
import 'package:provider/provider.dart';
import 'package:rei_app/models/auth/index.dart';
import 'package:rei_app/provider/factory_contract_provider.dart';
import 'package:rei_app/provider/smart_contract_token_provider.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:rei_app/provider/trade_contract_provider.dart';
import 'package:rei_app/provider/user_provider.dart';
import 'package:rei_app/utils/rei_colors.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: const FirebaseOptions(
    projectId: 'real-estate-investing-f2aa3',
    apiKey: 'AIzaSyAffjY0QJAd73UorA3f1Tw1aipYgdEzZlk',
    messagingSenderId: '273389952366',
    appId: '1:273389952366:android:850f504a81e86cd943b57f',
  ));

  await Hive.initFlutter();
  Hive.registerAdapter(AppUserAdapter());

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => SmartContractTokenProvider()),
        ChangeNotifierProvider(create: (_) => FactoryContractProvider()),
        ChangeNotifierProvider(create: (_) => UserProvider()),
        ChangeNotifierProvider(create: (_) => TradeContractProvider()),
      ],
      child: const ReiApp(),
    ),
  );
}

class ReiApp extends StatelessWidget {
  const ReiApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Lucrare Licenta',
      theme: ThemeData(
        appBarTheme: AppBarTheme(
          backgroundColor: Colors.transparent,
          foregroundColor: ReiColors.gunmetal,
          elevation: 0,
        ),
        primaryColor: ReiColors.gunmetal,
        textButtonTheme: TextButtonThemeData(
          style: ButtonStyle(
            foregroundColor: MaterialStateProperty.all(ReiColors.gunmetal),
          ),
        ),
        elevatedButtonTheme: ElevatedButtonThemeData(
          style: ButtonStyle(
            backgroundColor: MaterialStateProperty.all(ReiColors.gunmetal),
            shape: MaterialStateProperty.all(
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
            ),
            padding: MaterialStateProperty.all(
              const EdgeInsets.symmetric(horizontal: 20, vertical: 12),
            ),
          ),
        ),

      ),
      // initialRoute: AppRoutes.home,
      routes: AppRoutes.routes,
    );
  }
}
