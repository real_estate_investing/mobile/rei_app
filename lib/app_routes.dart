
// ignore: avoid_classes_with_only_static_members
import 'package:flutter/widgets.dart';
import 'package:rei_app/app/auth/auth_screen.dart';
import 'package:rei_app/app/homepage.dart';
import 'package:rei_app/app/invest/pages/buy_page.dart';
import 'package:rei_app/app/invest/pages/invest_details_page.dart';
import 'package:rei_app/app/send_transaction_page.dart';
import 'package:rei_app/home.dart';
import 'package:rei_app/splash_screen.dart';

class AppRoutes {
  static String home = '/';
  static String investWidgetPage = '/investWidgetPage';
  static String sendTransaction = '/sendTransaction';
  static String buyPage = '/buyPage';
  static String register = '/register';
  static String login = '/login';
  static String metamask = '/metamask';


  static Map<String, WidgetBuilder> routes = <String, WidgetBuilder>{
    home: (BuildContext context) => const SplashScreen(),
    metamask: (BuildContext context) => const Home(),
    register: (BuildContext context) {
      return const AuthScreen(type: AuthType.signUp);
    },
    login: (BuildContext context) {
      return const AuthScreen(type: AuthType.singIn);
    },
    sendTransaction: (BuildContext context) => const SendTransactionPage(),
    buyPage: (BuildContext context) {
      final Map<String, dynamic> arg = ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
      return BuyPage(contractInfo: arg['contractInfo']);
    },
    investWidgetPage: (BuildContext context) {
      final Map<String, dynamic> arg = ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
      return InvestDetailsPage(tag: arg['tag'], contractInfo: arg['contractInfo'],);
    },
  };
}
