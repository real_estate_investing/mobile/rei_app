import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rei_app/app/invest/widgets/Invest_widget_item.dart';
import 'package:rei_app/models/contract/index.dart';
import 'package:rei_app/provider/user_provider.dart';
import 'package:rei_app/utils/rei_colors.dart';

class FavoriteList extends StatefulWidget {
  const FavoriteList({Key? key}) : super(key: key);

  @override
  _FavoriteListState createState() => _FavoriteListState();
}

class _FavoriteListState extends State<FavoriteList> {
  @override
  void initState() {
    context.read<UserProvider>().getFavoritesContractInfo(context: context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Selector<UserProvider, List<ContractInfo>?>(
        selector: (_, model) => model.favoriteContracts,
        builder: (_, data, __) {
          if (data == null || data.isEmpty) {
            return const Center(
              child: Text('You dont\'t have any favorites'),
            );
          }
          return SingleChildScrollView(
            child: Column(
              children: [
                const SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Row(
                    children: [
                      Text(
                        'Your favorites',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 30,
                          color: ReiColors.gunmetal,
                        ),
                      ),
                    ],
                  ),
                ),
                ...List.generate(
                  data.length,
                  (index) => ProjectWidgetItem(
                    tag: 'favorite_$index',
                    contractInfo: data[index],
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
