import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rei_app/app_routes.dart';
import 'package:rei_app/provider/user_provider.dart';
import 'package:rei_app/utils/rei_colors.dart';
import 'package:tuple/tuple.dart';

enum AuthType { singIn, signUp }

class AuthScreen extends StatefulWidget {
  const AuthScreen({Key? key, required this.type}) : super(key: key);

  final AuthType type;

  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {
  late final String _title;
  late final String _buttonText;

  bool _isObscure = true;
  bool _isLoading = false;

  final TextEditingController _email = TextEditingController();
  final TextEditingController _password = TextEditingController();

  @override
  void initState() {
    if (widget.type == AuthType.signUp) {
      _title = 'Nice to meet you!';
      _buttonText = 'Register';
    } else {
      _title = 'Welcome back!';
      _buttonText = 'Login';
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: SafeArea(
        child: Stack(
          alignment: Alignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Form(
                child: Column(
                  children: <Widget>[
                    Text(
                      _title,
                      style: const TextStyle(fontSize: 28),
                    ),
                    const Expanded(child: SizedBox()),
                    TextFormField(
                      controller: _email,
                      decoration: InputDecoration(
                        enabled: !_isLoading,
                        hintText: 'Email',
                      ),
                      keyboardType: TextInputType.emailAddress,
                      validator: (String? value) {
                        if (value == null || value.isEmpty) {
                          return 'Email cannot be empty';
                        } else {
                          if (!value.contains('@')) {
                            return 'Invalid email';
                          }
                        }
                        return null;
                      },
                    ),
                    const Expanded(child: SizedBox()),
                    TextFormField(
                      controller: _password,
                      decoration: InputDecoration(
                          enabled: !_isLoading,
                          hintText: 'Password',
                          suffixIcon: IconButton(
                            icon: _isObscure ? const Icon(Icons.visibility) : const Icon(Icons.visibility_off),
                            onPressed: () {
                              setState(() {
                                _isObscure = !_isObscure;
                              });
                            },
                          )),
                      keyboardType: TextInputType.emailAddress,
                      obscureText: _isObscure,
                      validator: (String? value) {
                        if (value == null || value.isEmpty) {
                          return 'Password cannot be empty';
                        } else {
                          if (!value.contains(RegExp(r'[0-9]'))  ) {
                            return 'Pass have to contain at least a digit';
                          }
                          if (!value.contains(RegExp(r'[A-Z]'))  ) {
                            return 'Pass have to contain at least a capitalize letter';
                          }
                          if (value.length < 6 ) {
                            return 'Pass have to be at least of length 6';
                          }

                        }
                        return null;
                      },
                    ),
                    if (widget.type == AuthType.singIn)
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: <Widget>[
                          TextButton(
                            onPressed: () {
                              if (!_isLoading) {
                                //
                              }
                            },
                            child: const Text('Forgot password'),
                          ),
                        ],
                      ),
                    const Expanded(flex: 4, child: SizedBox()),
                    Builder(
                      builder: (context) {
                        return ElevatedButton(
                          onPressed: () async {
                            if (!_isLoading) {
                              final bool valid = Form.of(context)?.validate() ?? false;
                              if (valid) {
                                setState(() {
                                  _isLoading = true;
                                });
                                late final Tuple2<bool, dynamic> result;
                                if (widget.type == AuthType.singIn) {
                                  result = await context
                                      .read<UserProvider>()
                                      .signIn(email: _email.text, password: _password.text);
                                } else {
                                  result = await context
                                      .read<UserProvider>()
                                      .signUp(email: _email.text, password: _password.text);
                                }
                                if (result.item1) {
                                  Navigator.popUntil(context, (route) => route.isFirst);
                                  Navigator.pushReplacementNamed(context, AppRoutes.metamask);
                                } else {
                                  showDialog(
                                    context: context,
                                    builder: (context) => AlertDialog(
                                      title: const Text('Error'),
                                      content: Text(result.item2),
                                    ),
                                  );
                                  setState(() {
                                    _isLoading = false;
                                  });
                                }
                              }
                            }
                          },
                          child: Text(_buttonText),
                        );
                      },
                    ),
                    const Expanded(flex: 4, child: SizedBox()),
                    if (widget.type == AuthType.singIn)
                      TextButton(
                        onPressed: () {
                          if (!_isLoading) {
                            Navigator.pushNamed(context, AppRoutes.register);
                          }
                        },
                        child: const Text.rich(
                          TextSpan(
                            text: 'Don\'t have an account? ',
                            children: [
                              TextSpan(
                                text: 'Sign up',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                            ],
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ),
            // if (_isLoading)
            //   Container()
            if (_isLoading)
              CircularProgressIndicator(
                color: ReiColors.gunmetal,
              )
          ],
        ),
      ),
    );
  }
}
