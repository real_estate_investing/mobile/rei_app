import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class SendTransactionPage extends StatefulWidget {
  const SendTransactionPage({Key? key}) : super(key: key);

  @override
  _SendTransactionPageState createState() => _SendTransactionPageState();
}

class _SendTransactionPageState extends State<SendTransactionPage> {
  final nrOfTokens = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Column(
        children: <Widget>[
          TextField(
            controller: nrOfTokens,
          ),
          ElevatedButton(
              onPressed: () {
                print('BUY ${nrOfTokens.text}');
              },
              child: const Text("BUY"))
        ],
      ),
    );
  }
}
