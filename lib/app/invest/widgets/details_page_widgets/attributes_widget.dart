import 'package:flutter/material.dart';
import 'package:rei_app/utils/rei_colors.dart';

class AttributesWidget extends StatelessWidget {
  const AttributesWidget({Key? key, required this.icon, required this.title, required this.value}) : super(key: key);

  final IconData? icon;
  final String? title;
  final String? value;

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        if (icon != null)
          Icon(
            icon,
            size: 32,
            color: ReiColors.middleBlue,
          ),
        if (icon != null)
          const SizedBox(width: 12),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              title ?? 'Unknown',
              style: const TextStyle(
                color: Colors.grey,
                fontSize: 12,
              ),
            ),
            Text(
              value ?? 'Unknown',
              style: const TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        )
      ],
    );
  }
}
