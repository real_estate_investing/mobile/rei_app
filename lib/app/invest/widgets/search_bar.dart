import 'package:flutter/material.dart';
import 'package:rei_app/utils/rei_colors.dart';

class SearchBar extends StatefulWidget {
  const SearchBar({Key? key}) : super(key: key);

  @override
  _SearchBarState createState() => _SearchBarState();
}

class _SearchBarState extends State<SearchBar> {
  final _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return ListTile(
      dense: true,
      tileColor: ReiColors.whiteSmoke,
      contentPadding: const EdgeInsets.only(left: 8, right: 4),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(8.0),
      ),
      leading: const Icon(
        Icons.search,
        size: 16,
      ),
      horizontalTitleGap: 0,
      enableFeedback: true,
      title: const TextField(
        decoration: InputDecoration(
          isCollapsed: true,
          hintText: 'Search',
          hintStyle: TextStyle(fontSize: 12),
          border: InputBorder.none,
        ),
      ),
      trailing: Container(
          height: 32,
          width: 32,
          decoration: BoxDecoration(
            color: ReiColors.smokeBlack,
            borderRadius: BorderRadius.circular(6),
          ),
          child: Icon(
            Icons.filter_list,
            color: ReiColors.ghostWhite,
            size: 16,
          )),
    );
  }
}
