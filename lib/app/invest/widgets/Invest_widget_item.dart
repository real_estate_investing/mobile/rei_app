import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:rei_app/app_routes.dart';
import 'package:rei_app/models/auth/index.dart';
import 'package:rei_app/models/contract/index.dart';
import 'package:rei_app/provider/smart_contract_token_provider.dart';
import 'package:rei_app/provider/user_provider.dart';
import 'package:rei_app/utils/rei_colors.dart';
import 'package:provider/provider.dart';
import 'package:tuple/tuple.dart';
import 'package:web3dart/credentials.dart';

class ProjectWidgetItem extends StatefulWidget {
  const ProjectWidgetItem({Key? key, required this.tag, required this.contractInfo}) : super(key: key);

  final String tag;
  final ContractInfo contractInfo;

  @override
  State<ProjectWidgetItem> createState() => _ProjectWidgetItemState();
}

class _ProjectWidgetItemState extends State<ProjectWidgetItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        Navigator.of(context, rootNavigator: true).pushNamed(
          AppRoutes.investWidgetPage,
          arguments: {
            'tag': widget.tag,
            'contractInfo': widget.contractInfo,
          },
        );
      },
      child: Container(
        // height: MediaQuery.of(context).size.height * 0.6,
        margin: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        // width: double.infinity,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: <Widget>[
            Hero(
              tag: widget.tag,
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: CachedNetworkImage(
                  imageUrl:
                      'https://gateway.pinata.cloud/ipfs/${widget.contractInfo.details.folderCID}/${widget.contractInfo.details.images?[0]}',
                  fit: BoxFit.cover,
                  height: MediaQuery.of(context).size.height * 0.6,
                  placeholder: (_, __) => Container(
                    color: ReiColors.gunmetal,
                  ),
                  errorWidget: (_, __, ___) => Container(
                    color: ReiColors.gunmetal,
                    child: const Center(
                      child: Text('Something went wrong'),
                    ),
                  ),
                ),
              ),
            ),
            Container(
              height: 100,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                gradient: LinearGradient(
                  colors: <Color>[Colors.black.withOpacity(0.0), Colors.black],
                  begin: Alignment.topCenter,
                  end: FractionalOffset.bottomCenter,
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Row(
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 20.0),
                            child: Text(
                              widget.contractInfo.details.city ?? 'Unknown',
                              style: const TextStyle(
                                color: Colors.white,
                                fontSize: 28,
                                fontWeight: FontWeight.w600,
                              ),
                            ),
                          ),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 12),
                        child: Text(
                          widget.contractInfo.details.address ?? 'Unknown',
                          style: const TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                            fontWeight: FontWeight.w400,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const Spacer(),
                  Selector<UserProvider, AppUser?>(
                    selector: (_, model) => model.appUser,
                    builder: (_, data, __) {
                      bool isNotFav = (data == null || !data.favorites.contains(widget.contractInfo.address.toString()));
                      return GestureDetector(
                          onTap: () async {
                            final Tuple2<bool, String?> success = await context
                                .read<UserProvider>()
                                .updateFavorite(context: context, address: widget.contractInfo.address.hex);
                            // setState(() {
                            //
                            // });
                            if (!success.item1) {
                              showDialog(
                                context: context,
                                builder: (context) => AlertDialog(
                                  title: const Text('Error'),
                                  content: Text(success.item2 ?? ''),
                                ),
                              );
                            }
                          },
                          child: AnimatedContainer(
                            duration: const Duration(milliseconds: 100),
                            height: 44,
                            width: 44,
                            padding: const EdgeInsets.all(4),
                            margin: const EdgeInsets.all(8),
                            decoration: BoxDecoration(
                              color: isNotFav ? ReiColors.gunmetal.withOpacity(0.3) : ReiColors.whiteSmoke,
                              shape: BoxShape.circle,
                            ),
                            child: Container(
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: ReiColors.ashGrey.withOpacity(0.6),
                              ),
                              child: isNotFav
                                  ? Icon(
                                      Icons.favorite_border,
                                      color: ReiColors.ghostWhite,
                                    )
                                  : Icon(
                                      Icons.favorite_rounded,
                                      color: ReiColors.vividBurgundy,
                                    ),
                            ),
                          ),);
                    },
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
