import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rei_app/app/invest/widgets/Invest_widget_item.dart';
import 'package:rei_app/app/invest/widgets/search_bar.dart';
import 'package:rei_app/models/contract/index.dart';
import 'package:rei_app/provider/factory_contract_provider.dart';
import 'package:rei_app/provider/smart_contract_token_provider.dart';
import 'package:rei_app/utils/rei_colors.dart';

class InvestList extends StatefulWidget {
  const InvestList({Key? key}) : super(key: key);

  @override
  _InvestListState createState() => _InvestListState();
}

class _InvestListState extends State<InvestList> {
  Future? _future;

  @override
  void initState() {
    // _future = context.read<SmartContractTokenProvider>().getAllContractsInfo(context: context);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: RefreshIndicator(
        onRefresh: ()async{
          await context.read<SmartContractTokenProvider>().getAllContractsInfo(context: context);
        },
        child: CustomScrollView(
          slivers: [
            SliverAppBar(
              automaticallyImplyLeading: false,
              // pinned: true,
              floating: true,
              toolbarHeight: 100,
              // expandedHeight: 150,
              backgroundColor: ReiColors.ghostWhite,
              flexibleSpace: const FlexibleSpaceBar(
                title: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.0),
                  child: SearchBar(),
                ),
                centerTitle: true,
              ),
            ),
            Selector<SmartContractTokenProvider, List<ContractInfo>?>(
                selector: (_, model) => model.allContractsInfo,
                builder: (_, data, __) {
                  if (data == null) {
                    return SliverList(
                      delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) {
                          return const Center(child: CupertinoActivityIndicator());
                        },
                        childCount: 1,
                      ),
                    );
                  }
                  final List<ContractInfo> result = data;
                  return SliverList(
                    delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) {
                        return Column(
                          children: [
                            ProjectWidgetItem(
                              tag: 'investing_widget_item_$index',
                              contractInfo: result[index],
                            ),
                            const SizedBox(height: 20),
                          ],
                        );
                      },
                      childCount: result.length,
                    ),
                  );
                }),
          ],
        ),
      ),
    );
  }
}
