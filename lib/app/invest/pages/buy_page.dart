import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:rei_app/models/contract/index.dart';
import 'package:rei_app/provider/smart_contract_token_provider.dart';
import 'package:rei_app/utils/rei_colors.dart';

class BuyPage extends StatefulWidget {
  const BuyPage({Key? key, required this.contractInfo}) : super(key: key);

  final ContractInfo contractInfo;

  @override
  _BuyPageState createState() => _BuyPageState();
}

class _BuyPageState extends State<BuyPage> {
  final TextEditingController _controller = TextEditingController();
  String? errorText;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: GestureDetector(
          onTap: () => Navigator.pop(context),
          child: Container(
            height: 40,
            width: 40,
            padding: const EdgeInsets.all(4),
            margin: const EdgeInsets.all(8),
            decoration: BoxDecoration(
              color: ReiColors.whiteSmoke,
              shape: BoxShape.circle,
            ),
            child: Container(
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.white,
              ),
              child: Icon(
                Icons.arrow_back_ios_rounded,
                color: ReiColors.middleBlue,
              ),
            ),
          ),
        ),
      ),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Text(
              'Buy Tokens',
              style: TextStyle(
                color: ReiColors.gunmetal,
                fontSize: 28,
              ),
            ),
            const SizedBox(height: 20),
            Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Text(
                    'Enter the amount in ether:',
                    style: TextStyle(
                      color: ReiColors.gunmetal.withOpacity(0.5),
                      fontSize: 16,
                    ),
                  ),
                ),
              ],
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: TextField(
                controller: _controller,
                decoration: InputDecoration(
                  hintText: 'amount',
                  suffixText: 'ether',
                  errorText: errorText,
                ),
                keyboardType: TextInputType.phone,
                inputFormatters: [FilteringTextInputFormatter.allow(RegExp(r'^\d+\.?\d{0,18}'))],
                onChanged: (value) {
                  setState(() {});
                },
              ),
            ),
            const Expanded(child: SizedBox()),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'nr of tokens',
                    style: TextStyle(
                      color: ReiColors.gunmetal.withOpacity(0.5),
                    ),
                  ),
                  Text(
                    '${BigInt.from(int.tryParse(_controller.text) ?? 0) * widget.contractInfo.tokenPrice}',
                    style: TextStyle(
                      color: ReiColors.gunmetal.withOpacity(0.5),
                    ),
                  ),
                ],
              ),
            ),
            const Expanded(flex: 2, child: SizedBox()),
            ElevatedButton(
              onPressed: () async {
                final tokenAmount = double.tryParse(_controller.text);
                if (tokenAmount == null) {
                  errorText = 'Invalid amount of ether';
                } else {
                  final int tokenAmountToWei = (tokenAmount * pow(10, 18)).toInt();
                  final String tx = await context
                      .read<SmartContractTokenProvider>()
                      .buyTokens(contractAddress: widget.contractInfo.address, amount: tokenAmountToWei);
                  print('Tx BUY: $tx');
                }
                setState(() {});
              },
              style: const ButtonStyle().copyWith(
                  backgroundColor: MaterialStateProperty.all(ReiColors.gunmetal),
                  shape: MaterialStateProperty.all(
                    RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
                  ),
                  padding: MaterialStateProperty.all(const EdgeInsets.symmetric(horizontal: 20, vertical: 12))),
              child: Text(
                'BUY',
                style: TextStyle(
                  color: ReiColors.ghostWhite,
                  fontSize: 20,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
            const Expanded(flex: 3, child: SizedBox()),
          ],
        ),
      ),
    );
  }
}
