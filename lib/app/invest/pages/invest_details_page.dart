import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';
import 'package:rei_app/app/invest/widgets/details_page_widgets/attributes_widget.dart';
import 'package:rei_app/app_routes.dart';
import 'package:rei_app/models/contract/index.dart';
import 'package:rei_app/provider/smart_contract_token_provider.dart';
import 'package:rei_app/utils/rei_colors.dart';

class InvestDetailsPage extends StatefulWidget {
  const InvestDetailsPage({Key? key, required this.tag, required this.contractInfo}) : super(key: key);
  final String tag;
  final ContractInfo contractInfo;

  @override
  State<InvestDetailsPage> createState() => _InvestDetailsPageState();
}

class _InvestDetailsPageState extends State<InvestDetailsPage> {
  final ScrollController _scrollController = ScrollController();
  bool scrolled = false;
  double _offset = 0;

  bool _isVisibleTextDescription = false;
  Stream? _stream;
  Stream? _stream2;

  @override
  void initState() {
    _stream = context
        .read<SmartContractTokenProvider>()
        .getAvailableTokensStream(contractAddress: widget.contractInfo.address);
    _stream2 =
        context.read<SmartContractTokenProvider>().getOwnedTokensStream(contractAddress: widget.contractInfo.address);
    _scrollController.addListener(() {
      setState(() {
        _offset = _scrollController.offset - _scrollController.initialScrollOffset;
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: true,
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.all(12.0),
          child: Stack(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Expanded(
                    child: SingleChildScrollView(
                      controller: _scrollController,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Stack(
                            alignment: Alignment.bottomCenter,
                            children: <Widget>[
                              Hero(
                                tag: widget.tag,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(20),
                                  child: CachedNetworkImage(
                                    imageUrl: 'https://gateway.pinata.cloud/ipfs/${widget.contractInfo.details.folderCID}/${widget.contractInfo.details.images?[0]}',
                                    fit: BoxFit.cover,
                                    height: MediaQuery.of(context).size.height * 0.6,
                                    placeholder: (_, __) => Container(
                                      color: ReiColors.gunmetal,
                                    ),
                                    errorWidget: (_, __, ___) => Container(
                                      color: ReiColors.gunmetal,
                                      child: const Center(
                                        child: Text('Something went wrong'),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                height: 75,
                                width: MediaQuery.of(context).size.width,
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(20),
                                  gradient: LinearGradient(
                                    colors: <Color>[Colors.black.withOpacity(0.0), Colors.black],
                                    begin: Alignment.topCenter,
                                    end: FractionalOffset.bottomCenter,
                                  ),
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(12.0),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      children: [
                                        Padding(
                                          padding: const EdgeInsets.symmetric(horizontal: 20.0),
                                          child: Text(
                                            widget.contractInfo.details.city ?? 'Unknown',
                                            style: const TextStyle(
                                              color: Colors.white,
                                              fontSize: 28,
                                              fontWeight: FontWeight.w600,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 12),
                                      child: Text(
                                        widget.contractInfo.details.address ?? 'Unknown',
                                        style: const TextStyle(
                                          color: Colors.white,
                                          fontSize: 12,
                                          fontWeight: FontWeight.w400,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 10.0, vertical: 20),
                            child: Text(
                              'Description',
                              style: TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w800,
                                color: ReiColors.smokeBlack,
                              ),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: <Widget>[
                              AttributesWidget(
                                  title: 'Area',
                                  value: widget.contractInfo.details.area.toString(),
                                  icon: Icons.widgets_outlined),
                              AttributesWidget(
                                  title: 'Type',
                                  value: widget.contractInfo.details.typeOfBuilding,
                                  icon: Icons.business_sharp),
                            ],
                          ),
                          Padding(
                            padding: const EdgeInsets.all(20.0),
                            child: GestureDetector(
                              onTap: () {
                                setState(() {
                                  _isVisibleTextDescription = true;
                                });
                              },
                              child: Text(
                                widget.contractInfo.details.description ?? 'Unknown',
                                maxLines: _isVisibleTextDescription ? null : 3,
                                overflow: _isVisibleTextDescription ? null : TextOverflow.ellipsis,
                                style: TextStyle(
                                  color: ReiColors.gunmetal.withOpacity(0.5),
                                ),
                              ),
                            ),
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Column(
                                children: <Widget>[
                                  const Text(
                                    'Tokens left',
                                    style: TextStyle(fontSize: 28),
                                  ),
                                  const SizedBox(height: 12),
                                  StreamBuilder(
                                    stream: _stream,
                                    builder: (context, snapshot) {
                                      if (snapshot.connectionState == ConnectionState.waiting) {
                                        return const Text('...');
                                      }
                                      final BigInt tokens = snapshot.data as BigInt;
                                      return Text(
                                        (tokens / BigInt.from(pow(10, 18))).toString(),
                                        style: const TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                                      );
                                    },
                                  ),
                                ],
                              ),
                            ],
                          ),
                          const SizedBox(height: 12),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Column(
                                children: <Widget>[
                                  const Text(
                                    'Tokens owned',
                                    style: TextStyle(fontSize: 28),
                                  ),
                                  const SizedBox(height: 12),
                                  StreamBuilder(
                                    stream: _stream2,
                                    builder: (context, snapshot) {
                                      if (snapshot.connectionState == ConnectionState.waiting) {
                                        return const Text('...');
                                      }
                                      final BigInt tokens = snapshot.data as BigInt;
                                      if (tokens == BigInt.zero) {
                                        return const Text('You dont own tokens');
                                      }
                                      return Text(
                                        (tokens / BigInt.from(pow(10, 18))).toString(),
                                        style: const TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                                      );
                                    },
                                  ),
                                ],
                              ),
                            ],
                          ),
                          const SizedBox(height: 20),
                        ],
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            'Price/Token',
                            style: TextStyle(color: ReiColors.gunmetal.withOpacity(0.5), fontSize: 16),
                          ),
                          Text(
                            '${widget.contractInfo.tokenPrice} ether',
                            style: const TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                          ),
                        ],
                      ),
                      ElevatedButton(
                        onPressed: () {
                          Navigator.pushNamed(context, AppRoutes.buyPage,
                              arguments: {'contractInfo': widget.contractInfo});
                        },
                        style: const ButtonStyle().copyWith(
                            backgroundColor: MaterialStateProperty.all(ReiColors.gunmetal),
                            shape: MaterialStateProperty.all(
                              RoundedRectangleBorder(borderRadius: BorderRadius.circular(18)),
                            ),
                            padding:
                                MaterialStateProperty.all(const EdgeInsets.symmetric(horizontal: 20, vertical: 12))),
                        child: Text(
                          'Continue',
                          style: TextStyle(
                            color: ReiColors.ghostWhite,
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              AnimatedContainer(
                duration: const Duration(milliseconds: 200),
                decoration: BoxDecoration(
                  color:
                      MediaQuery.of(context).size.height * 0.6 - _offset < 60 ? ReiColors.gunmetal : Colors.transparent,
                  borderRadius: BorderRadius.circular(20),
                ),
                child: Row(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () => Navigator.pop(context),
                      child: Container(
                        height: 40,
                        width: 40,
                        padding: const EdgeInsets.all(4),
                        margin: const EdgeInsets.all(8),
                        decoration: BoxDecoration(
                          color: ReiColors.whiteSmoke,
                          shape: BoxShape.circle,
                        ),
                        child: Container(
                          decoration: const BoxDecoration(
                            shape: BoxShape.circle,
                            color: Colors.white,
                          ),
                          child: Icon(
                            Icons.arrow_back_ios_rounded,
                            color: ReiColors.middleBlue,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
