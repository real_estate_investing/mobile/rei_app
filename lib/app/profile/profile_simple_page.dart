import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rei_app/app_routes.dart';
import 'package:rei_app/models/auth/index.dart';
import 'package:rei_app/provider/smart_contract_token_provider.dart';
import 'package:rei_app/provider/user_provider.dart';
import 'package:rei_app/utils/rei_colors.dart';
import 'package:web3dart/src/credentials/address.dart';

class ProfileSimplePage extends StatefulWidget {
  const ProfileSimplePage({Key? key}) : super(key: key);

  @override
  _ProfileSimplePageState createState() => _ProfileSimplePageState();
}

class _ProfileSimplePageState extends State<ProfileSimplePage> {
  @override
  Widget build(BuildContext context) {
    final AppUser? user = context.watch<UserProvider>().appUser;
    final EthereumAddress? address = context.watch<SmartContractTokenProvider>().userWalletAddress;
    if (user == null) {
      return const Center(child: Text('You have to be logged in'));
    }
    return Column(
      children: [
        const Expanded(flex: 1, child: SizedBox()),
        Text(
          'Your Profile',
          style: TextStyle(
            color: ReiColors.gunmetal,
            fontSize: 30,
            fontWeight: FontWeight.bold,
          ),
        ),
        const Expanded(flex: 2, child: SizedBox()),
        Text('Email',
            style: TextStyle(
              color: ReiColors.gunmetal,
              fontSize: 30,
              fontWeight: FontWeight.bold,
            )),
        Text(user.email),
        const Expanded(flex: 2, child: SizedBox()),
        Text('Connected address',
            style: TextStyle(
              color: ReiColors.gunmetal,
              fontSize: 30,
              fontWeight: FontWeight.bold,
            )),
        Text(address?.hex ?? 'Not connected'),
        const Expanded(flex: 3, child: SizedBox()),
        ElevatedButton(
          onPressed: () async {
            Navigator.pushReplacementNamed(context, AppRoutes.login);
            await context.read<UserProvider>().signOut();
          },
          child: Row(
            mainAxisSize: MainAxisSize.min,
            children: const [
              Text('Sign Out'),
              SizedBox(width: 12),
              Icon(Icons.logout),
            ],
          ),
        ),
      ],
    );
  }
}
