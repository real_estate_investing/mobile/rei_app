import 'package:flutter/material.dart';
import 'package:rei_app/utils/rei_colors.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: ReiColors.milk,
      appBar: AppBar(
        backgroundColor: ReiColors.milk,
        shadowColor: Colors.transparent,
        actions: [
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.settings,
              color: ReiColors.eerieBlack,
            ),
          ),
          IconButton(
            onPressed: () {},
            icon: Icon(
              Icons.help,
              color: ReiColors.eerieBlack,
            ),
          ),
        ],
      ),
      body: SingleChildScrollView(
        child: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              const SizedBox(width: double.infinity),
              Container(
                height: 132,
                width: 132,
                padding: const EdgeInsets.all(20.0),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                  color: Colors.white,
                  boxShadow: const [
                    BoxShadow(),
                  ],
                ),
                child: const Icon(
                  Icons.account_circle,
                  size: 48,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: const Text('@bogdy9912'),
              ),
              Container(
                padding: const EdgeInsets.symmetric(vertical: 12, horizontal: 20),
                decoration: BoxDecoration(borderRadius: BorderRadius.circular(12), color: ReiColors.white),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Column(
                      children: const <Widget>[
                        Text('12314'),
                        Text('Globa rank'),
                      ],
                    ),
                    const CircleAvatar(),
                    Column(
                      children: const <Widget>[
                        Text('200'),
                        Text('friends'),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Row(
                  children: const <Widget>[
                    Text('MY STICKERS'),
                  ],
                ),
              ),
              SizedBox(
                height: 170,
                child: ListView.separated(
                  separatorBuilder: (BuildContext context, int index) => const SizedBox(width: 12),
                  scrollDirection: Axis.horizontal,
                  itemCount: 4,
                  itemBuilder: (BuildContext context, int index) => Row(
                    children: <Widget>[
                      if (index == 0) const SizedBox(width: 20),
                      Container(
                        height: 170,
                        width: 130,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: ReiColors.palish,
                        ),
                      ),
                      if (index == 3) const SizedBox(width: 20),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(20.0),
                child: Row(
                  children: const <Widget>[
                    Text('MY GOAL'),
                  ],
                ),
              ),
              Container(
                height: 400,
                width: double.infinity,
                margin: const EdgeInsets.all(20),
                decoration: BoxDecoration(
                  color: ReiColors.borderLight,
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
