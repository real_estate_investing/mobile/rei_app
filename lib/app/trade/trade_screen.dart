import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rei_app/app/trade/widgets/trade_widget_item.dart';
import 'package:rei_app/models/contract/deposit_model.dart';
import 'package:rei_app/models/contract/index.dart';
import 'package:rei_app/provider/smart_contract_token_provider.dart';
import 'package:rei_app/provider/trade_contract_provider.dart';
import 'package:rei_app/utils/rei_colors.dart';
import 'package:tuple/tuple.dart';

class TradeScreen extends StatefulWidget {
  const TradeScreen({Key? key}) : super(key: key);

  @override
  _TradeScreenState createState() => _TradeScreenState();
}

class _TradeScreenState extends State<TradeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Selector<TradeContractProvider, List<DepositModel>?>(
        selector: (_, model) => model.deposits,
        builder: (_, data, __) => Column(
          children: [

            const SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.all(20.0),
              child: Row(
                children: [
                  Text(
                    'Marketplace',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 30,
                      color: ReiColors.gunmetal,
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: RefreshIndicator(
                onRefresh: () async => context.read<TradeContractProvider>().getAllDeposits(),
                child: ListView.builder(
                  itemCount: data?.length??0,
                  itemBuilder: (context, index) {
                    if (data == null){
                      return const Text('No offers to display');
                    }
                    final ContractInfo? depositContractInfo = context.read<SmartContractTokenProvider>().allContractsInfo?.firstWhere((element) => element.address == data[index].contractAddress);

                    // de vazut cand e null
                    return TradeWidgetItem(deposit: data[index], contractInfo: depositContractInfo);
                  }
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
