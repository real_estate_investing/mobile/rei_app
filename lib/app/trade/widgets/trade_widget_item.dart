import 'dart:math';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rei_app/models/contract/deposit_model.dart';
import 'package:rei_app/models/contract/index.dart';
import 'package:rei_app/provider/smart_contract_token_provider.dart';
import 'package:rei_app/provider/trade_contract_provider.dart';
import 'package:rei_app/utils/rei_colors.dart';

class TradeWidgetItem extends StatefulWidget {
  const TradeWidgetItem({Key? key, required this.deposit, required this.contractInfo}) : super(key: key);

  final DepositModel deposit;
  final ContractInfo? contractInfo;

  @override
  _TradeWidgetItemState createState() => _TradeWidgetItemState();
}

class _TradeWidgetItemState extends State<TradeWidgetItem> {
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(20),
      padding: const EdgeInsets.all(8),
      width: double.infinity,
      height: 200,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        border: Border.all(
          color: ReiColors.middleBlue,
          width: 4,
        ),
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20),
              child: CachedNetworkImage(
                  width: 100,
                  height: double.infinity,
                  fit: BoxFit.cover,
                  imageUrl:
                      'https://gateway.pinata.cloud/ipfs/${widget.contractInfo?.details.folderCID}/${widget.contractInfo?.details.images?[0]}'),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text(
                  'Project name',
                  style: TextStyle(color: ReiColors.gunmetal, fontWeight: FontWeight.bold),
                ),
                Text('${widget.contractInfo?.details.name}'),
                Text(
                  'Amount of tokens sold',
                  style: TextStyle(color: ReiColors.gunmetal, fontWeight: FontWeight.bold),
                ),
                Text('${widget.deposit.amount / BigInt.from(pow(10, 18))}'),
                Text(
                  'Price per token',
                  style: TextStyle(color: ReiColors.gunmetal, fontWeight: FontWeight.bold),
                ),
                Text('${widget.deposit.pricePerToken}'),
                Row(
                  children: <Widget>[
                    ElevatedButton(
                        onPressed: () async {
                          final bool isOnWhitelist = await context
                              .read<SmartContractTokenProvider>()
                              .isOnWhitelist(contractAddress: widget.deposit.contractAddress);

                          if (isOnWhitelist) {
                            // show to buy
                            
                          } else {
                            // need approve
                            showDialog(
                                context: context,
                                builder: (context) => const AlertDialog(
                                      title: Text('Whitelist'),
                                      content: Text(
                                          'You have to apply and register on the project whitelist in order to own security tokens.'),
                                    ));
                          }
                        },
                        child: Text('Buy')),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
    //   Container(
    //   margin: const EdgeInsets.all(20),
    //   height: 200,
    //   width: double.infinity,
    //   color: Colors.red,
    //   child:
    //
    //
    //   Stack(
    //     children: [
    //       CachedNetworkImage(fit: BoxFit.cover,
    //           imageUrl:
    //               'https://gateway.pinata.cloud/ipfs/${widget.contractInfo?.details.folderCID}/${widget.contractInfo?.details.images?[0]}'),
    //       Column(
    //         children: <Widget>[
    //           Text('${widget.deposit.amount}'),
    //           Text('${widget.deposit.pricePerToken}'),
    //         ],
    //       ),
    //     ],
    //   ),
    // );
  }
}
