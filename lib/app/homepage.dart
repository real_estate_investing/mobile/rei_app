import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:rei_app/app/favorite/favorite_list.dart';
import 'package:rei_app/app/invest/invest_list.dart';
import 'package:rei_app/app/profile/profile_page.dart';
import 'package:rei_app/app/profile/profile_simple_page.dart';
import 'package:rei_app/app/trade/trade_screen.dart';
import 'package:rei_app/provider/smart_contract_token_provider.dart';
import 'package:rei_app/provider/trade_contract_provider.dart';
import 'package:rei_app/utils/rei_colors.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 0;

  @override
  void initState() {
    context.read<SmartContractTokenProvider>().getAllContractsInfo(context: context);
    context.read<TradeContractProvider>().getAllDepositsContract(context: context);
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: <Widget>[
            Expanded(
              child: [
                const InvestList(),
                const FavoriteList(),
                const TradeScreen(),
                const ProfileSimplePage(),
              ][_selectedIndex],
            ),
            Container(
              width: double.infinity,
              margin: const EdgeInsets.only(left: 20, right: 20, bottom: 20, top: 10),
              padding: const EdgeInsets.only(bottom: 10, top: 10),
              decoration: BoxDecoration(
                color: ReiColors.smokeBlack,
                borderRadius: BorderRadius.circular(20),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  IconButton(
                    onPressed: () {
                      setState(() {
                        _selectedIndex = 0;
                      });
                    },
                    icon: const Icon(
                      Icons.home,
                      color: Colors.white,
                    ),
                  ),
                  IconButton(
                    onPressed: () {
                      setState(() {
                        _selectedIndex = 1;
                      });
                    },
                    icon: const Icon(
                      Icons.favorite,
                      color: Colors.white,
                    ),
                  ),
                  IconButton(
                    onPressed: () {
                      setState(() {
                      _selectedIndex = 2;

                      });
                    },
                    icon: const Icon(
                      Icons.switch_camera_outlined,
                      color: Colors.white,
                    ),
                  ),
                  IconButton(
                    onPressed: () {
                      setState(() {
                        _selectedIndex = 3;
                      });
                    },
                    icon: const Icon(
                      Icons.account_circle,
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
