import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:rei_app/WalletConnectEthereumCredentials.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:walletconnect_dart/walletconnect_dart.dart';
import 'dart:typed_data';
import 'package:web3dart/web3dart.dart';
import 'package:http/http.dart';
import 'models/smart_contracts/SecurityContract.g.dart';
import 'package:web_socket_channel/io.dart';
import 'package:path/path.dart' show join, dirname;

class WalletPage extends StatefulWidget {
  const WalletPage({Key? key}) : super(key: key);

  @override
  _WalletPageState createState() => _WalletPageState();
}

class _WalletPageState extends State<WalletPage> {
  final connector = WalletConnect(
    bridge: 'https://bridge.walletconnect.org',
    clientMeta: const PeerMeta(
      name: 'WalletConnect',
      description: 'WalletConnect Developer App',
      url: 'https://walletconnect.org',
      icons: ['https://gblobscdn.gitbook.com/spaces%2F-LJJeCjcLrr53DcT1Ml7%2Favatar.png?alt=media'],
    ),
  );

  // Subscribe to events

  // Create a new session

  Future<SessionStatus?>? session;
  String textResult = "";

  @override
  void initState() {
    connector.on('connect', (session) => print('session: $session'));
    connector.on('session_update', (payload) => print('payload: $payload'));
    connector.on('disconnect', (session) => print('disconnect: $session'));

    super.initState();
  }

  Future<void> ada() async {
    if (!connector.connected) {
      final session = await connector.createSession(
        chainId: 4160,
        onDisplayUri: (uri) {
          launchUrl(Uri.parse(uri));
        },
      );

      print(session);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
          child: Column(
        children: [
          SizedBox(height: 200),
          ElevatedButton(
              onPressed: () async {
                await ada();
              },
              child: Text('Connect')),
          ElevatedButton(
              onPressed: () async {
                print(connector.session.toJson());
                print(connector.session.accounts);
                print(connector.session.networkId);

                final String address = connector.session.accounts[0];
                const String secondAddress = "0x3b8d88190a2AFd1730353Df4FfAb5e7678CfE29d";
                print("DEBUG");
                String? result;
                try {
                  // print(Uint8List.fromList("0xd0febe4c".codeUnits));
                  // print("0xd0febe4c".codeUnits);
                  // result = await EthereumWalletConnectProvider(connector, chainId: 4).sendTransaction(
                  //     from: address, to: secondAddress, gas: 4000000, data: Uint8List.fromList("0xd0febe4c".codeUnits));
                } catch (err) {
                  print(err);
                }
                print("DEBUG2");
                print('result: $result');
                setState(() {
                  textResult = result ?? "asd";
                });

                print('DEBUG3');

                EthereumAddress cred = EthereumAddress.fromHex('0xc20276346b47E9D7d7d558F0387D147f26923Da6');
                print('DEBUG4');
                var apiUrl = "https://rinkeby.infura.io/v3/aef5a09436104568a84dff261b4deffc";
                print('DEBUG5');
                var httpClient = Client();
                print('DEBUG6');
                var ethClient = Web3Client(apiUrl, httpClient);
                print('DEBUG7');
                var balance = await ethClient.getBalance(cred);
                print('DEBUG8');
                print('balance: ${balance}');
                var wsUrl = 'wss://mainnet.infura.io/ws/v3/aef5a09436104568a84dff261b4deffc';
                final client = Web3Client(apiUrl, Client(), socketConnector: () {
                  return IOWebSocketChannel.connect(wsUrl).cast<String>();
                });
// final TransactionReceipt? ceva = await client.getTransactionReceipt("hash");
                print('DEBUG9');
                // print('DEBUG10');
                final String abiCode = await rootBundle.loadString('assets/SecurityContract.json');
                EthereumAddress contractAddr = EthereumAddress.fromHex('0x3b8d88190a2AFd1730353Df4FfAb5e7678CfE29d');
                // print('DEBUG11');
                // final contract =
                // DeployedContract(ContractAbi.fromJson(abiCode, 'MetaCoin'), contractAddr);
                print('DEBUG12');
                var totalSupp =
                    await SecurityContract(address: contractAddr, client: ethClient, chainId: 4).totalSupply();
                // EthPrivateKey key = EthPrivateKey.fromHex(ethKey.toString());
                // var totalSupp = await SecurityContract(address: contractAddr,client: ethClient, chainId: 4).exchage(_to, _amount, credentials: contractAddr);
                print('totalSupp: $totalSupp');
                print('DEBUG13');
                // final List chestie = await client.call(contract: DeployedContract(ContractAbi.fromJson(abiCode, 'SecurityContract'), contractAddr), function: ContractFunction('buyTokens', []), params: []);
                print('DEBUG14');
                // print(chestie);
                final contract =
                DeployedContract(ContractAbi.fromJson(abiCode, 'SecurityContract'), contractAddr);
                final ContractFunction contractFunction = contract.function('buyTokens');
                // print(contractFunction.encodeName().toUint8List());
                print(contractFunction.selector);
                final String alt = await WalletConnectEthereumCredentials(
                        wcProvider: EthereumWalletConnectProvider(connector, chainId: 4))
                    .sendTransaction(
                  Transaction(
                    from: cred,
                    to: contractAddr,
                    data: contractFunction.selector,
                    value: EtherAmount.inWei(BigInt.from(10000000000000)),
                  ),
                );
                print('DEBUG15');
                print(alt);
                print('DEBUG16');

//                     final algorand = Algorand(
//                       algodClient: AlgodClient(apiUrl: AlgoExplorer.TESTNET_ALGOD_API_URL),
//                     );
//
//                     final eth = WCEthereumTransaction(from: , to: to, gas: gas, data: data)
//
//                     WCEthereumSignMessage()
//
//                     EthTransaction
//
//                     final sender = Address.fromAlgorandAddress(address: connector.session.accounts[0]);
//
// // Fetch the suggested transaction params
//                     final params = await algorand.getSuggestedTransactionParams();
//
// // Build the transaction
//                     final tx = await (PaymentTransactionBuilder()
//                       ..sender = sender
//                       ..noteText = 'Signed with WalletConnect'
//                       ..amount = Algo.toMicroAlgos(0.0001)
//                       ..receiver = sender
//                       ..suggestedParams = params)
//                         .build();
              },
              child: Text('Print')),
          Text(textResult),
        ],
      )),
    );
  }
}

Future<void> connect() async {}
