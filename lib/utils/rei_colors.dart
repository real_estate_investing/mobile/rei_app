import 'dart:ui';

import 'package:flutter/material.dart';

class ReiColors{
  static Color black = HexColor.fromHex('#FFF6EE');
  static Color eerieBlack = HexColor.fromHex('#1B1B1B');
  static Color whiteSmoke = HexColor.fromHex('#efefef');
  static Color milk = HexColor.fromHex('#FFFAF5');
  static Color borderLight = HexColor.fromHex('#EFE3D3');
  static Color palish = HexColor.fromHex('#C4AA9C');
  static Color white = Colors.white;
  static Color smokeBlack = HexColor.fromHex('#121212');
  static Color ghostWhite = HexColor.fromHex('#FCFBFF');
  static Color middleBlue = HexColor.fromHex('#73BAC4');
  static Color ashGrey = HexColor.fromHex('#B3C1BD');
  static Color independence = HexColor.fromHex('#575C70');
  static Color gunmetal = HexColor.fromHex('#2A2D34');
  static Color vividBurgundy = HexColor.fromHex('#990D35');


}
  extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}
