import 'dart:convert';

import 'package:flutter/material.dart';
import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:rei_app/models/contract/index.dart';
import 'package:rei_app/models/smart_contracts/SecurityContract.g.dart';
import 'package:rei_app/WalletConnectEthereumCredentials.dart';
import 'package:rei_app/provider/factory_contract_provider.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:walletconnect_dart/walletconnect_dart.dart';
import 'dart:typed_data';
import 'package:web3dart/web3dart.dart';
import 'package:http/http.dart';
import 'package:web_socket_channel/io.dart';
import 'package:path/path.dart' show join, dirname;

class SmartContractTokenProvider extends ChangeNotifier {
  WalletConnect? connector;
  final String infuraUrl = "https://rinkeby.infura.io/v3/aef5a09436104568a84dff261b4deffc";
  Web3Client? ethClient;
  EthereumAddress? userWalletAddress;

  List<ContractInfo>? allContractsInfo;

  // final EthereumAddress contractAddress = EthereumAddress.fromHex('0x09B418394768841f094Df7d765BFd02aE2Fd2Bb2');
  DeployedContract? contract;
  WalletConnectEthereumCredentials? walletCredentials;

  Future<void> init() async {
    connector = WalletConnect(
      bridge: 'https://bridge.walletconnect.org',
      clientMeta: const PeerMeta(
        name: 'WalletConnect',
        description: 'WalletConnect Developer App',
        url: 'https://walletconnect.org',
        // icons: ['https://gblobscdn.gitbook.com/spaces%2F-LJJeCjcLrr53DcT1Ml7%2Favatar.png?alt=media'],
      ),
    );
    // connector!.registerListeners(onConnect: (status) {
    //   print('connected ${status}');
    // }, onDisconnect: () {
    //   print('Disconnected');
    // }, onSessionUpdate: (status) {
    //   print('Update session ${status}');
    // });
    ethClient = Web3Client(infuraUrl, Client());
    walletCredentials =
        WalletConnectEthereumCredentials(wcProvider: EthereumWalletConnectProvider(connector!, chainId: 4));
  }

  Future<void> loadAbiCode() async {
    final String abiCode = await rootBundle.loadString('assets/SecurityContract.json');
    contract = DeployedContract(ContractAbi.fromJson(abiCode, 'SecurityContract'),
        EthereumAddress.fromHex('0x2ef71507f3fa00536120f685dceb1f3532eb432e'));
  }

  Future<bool> connectWallet() async {
    SessionStatus? session;

    if (connector == null) {
      init();
    }

    session = await connector!.connect(
      chainId: 4,
      onDisplayUri: (uri) {
        launchUrl(Uri.parse(uri));
      },
    );

    userWalletAddress = EthereumAddress.fromHex(session.accounts[0]);
    notifyListeners();
    print(session);
    if (session.accounts.isEmpty) {
      throw Error();
    }

    return true;
  }

  Future<String> buyTokens({required int amount, required EthereumAddress contractAddress}) async {
    print(contract);
    if (contract == null) {
      await loadAbiCode();
    }
    final ContractFunction contractFunction = contract!.function('buyTokens');
    print('BOUGHT AMOUNT: $amount');
    print('BOUGHT AMOUNT in ETHER: ${EtherAmount.fromUnitAndValue(EtherUnit.wei, amount)}');
    print('userWalletAddress: ${userWalletAddress}');
    print('contractAddress: ${contractAddress}');
    print('contractFunction.selector: ${contractFunction.selector}');
    final String tx = await walletCredentials!.sendTransaction(
      Transaction(
        from: userWalletAddress,
        to: contractAddress,
        data: contractFunction.selector,
        value: EtherAmount.fromUnitAndValue(EtherUnit.wei, amount),
        // gasPrice: EtherAmount.inWei(BigInt.from(200000000000000)),


      ),
    );
    return tx;
  }

  Future<ContractDetails> getContractInfo({required EthereumAddress contractAddress}) async {
    final String contractUri =
        await SecurityContract(address: contractAddress, client: ethClient!, chainId: 4).contractURI();
    print(contractUri);
    final ContractDetails result = ContractDetails.fromJson(jsonDecode(contractUri));
    print(result);
    return result;
  }

  Future<List<ContractInfo>> getAllContractsInfo({required BuildContext context}) async {
    final List<EthereumAddress> contractsAddresses = await context.read<FactoryContractProvider>().getAllContracts();
    final result = await getContractsInfo(context: context, addresses: contractsAddresses);
    allContractsInfo = result;
    notifyListeners();
    return result;

  }

  Future<List<ContractInfo>> getContractsInfo({
    required BuildContext context,
    required List<EthereumAddress> addresses,
  }) async {
    List<ContractInfo> results = [];
    for (var element in addresses) {
      final BigInt availableTokens = await getAvailableTokens(contractAddress: element);
      final BigInt tokenPrice = await getTokenPrice(contractAddress: element);
      results.add(ContractInfo(
          details: await getContractInfo(contractAddress: element),
          address: element,
          availableTokens: availableTokens,
          tokenPrice: tokenPrice));
    }
    return results;
  }

  Future<BigInt> getAvailableTokens({required EthereumAddress contractAddress}) async {
    final BigInt availableTokens =
        await SecurityContract(address: contractAddress, client: ethClient!, chainId: 4).availableTokens();
    return availableTokens;
  }

  Future<BigInt> getTokenPrice({required EthereumAddress contractAddress}) async {
    final BigInt tokenPrice =
        await SecurityContract(address: contractAddress, client: ethClient!, chainId: 4).tokenPrice();
    return tokenPrice;
  }

  Stream<BigInt> getAvailableTokensStream({required EthereumAddress contractAddress}) async* {
    while (true) {
      await Future.delayed(const Duration(seconds: 1));
      yield await getAvailableTokens(contractAddress: contractAddress);
    }
  }

  Future<BigInt> getOwnedTokens({required EthereumAddress contractAddress}) async {
    final BigInt tokenOwned =
        await SecurityContract(address: contractAddress, client: ethClient!, chainId: 4).balanceOf(userWalletAddress!);
    return tokenOwned;
  }

  Stream<BigInt> getOwnedTokensStream({required EthereumAddress contractAddress}) async* {
    while (true) {
      await Future.delayed(const Duration(seconds: 1));
      yield await getOwnedTokens(contractAddress: contractAddress);
    }
  }


  Future<bool> isOnWhitelist({required EthereumAddress contractAddress})async{
    if (userWalletAddress == null){
      return false;
    }
    final bool result = await SecurityContract(address: contractAddress, client: ethClient!, chainId: 4).whitelist(userWalletAddress!);
    return result;
  }
}
