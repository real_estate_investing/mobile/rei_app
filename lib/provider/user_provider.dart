import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:provider/provider.dart';
import 'package:rei_app/models/auth/index.dart';
import 'package:rei_app/models/contract/index.dart';
import 'package:rei_app/provider/smart_contract_token_provider.dart';
import 'package:rei_app/services/auth_api.dart';
import 'package:tuple/tuple.dart';
import 'package:web3dart/credentials.dart';

class UserProvider extends ChangeNotifier {
  AppUser? appUser;
  Box<AppUser>? box;
  List<ContractInfo>? favoriteContracts;

  final AuthApi _authApi = AuthApi();

  Future<void> loadAppUser() async {
    await openBox();
    appUser = box!.get('appUser');
    notifyListeners();
  }

  Future<void> openBox() async {
    box = Hive.isBoxOpen('app_user') ? Hive.box('app_user') : await Hive.openBox('app_user');
  }

  Future<Tuple2<bool, String?>> signUp({required String email, required String password}) async {
    try {
      final AppUser newAppUser = await _authApi.registerEmailAndPassword(email: email, password: password);

      appUser = newAppUser;
      await openBox();
      await box!.put('appUser', appUser!);
    } catch (error) {
      return Tuple2(false, error.toString());
    } finally {
      notifyListeners();
    }
    return const Tuple2(true, null);
  }

  Future<Tuple2<bool, String?>> signIn({required String email, required String password}) async {
    try {
      final AppUser newAppUser = await _authApi.singInEmailAndPassword(email: email, password: password);

      appUser = newAppUser;
      await openBox();
      await box!.put('appUser', appUser!);
    } catch (error) {
      return Tuple2(false, error.toString());
    } finally {
      notifyListeners();
    }
    return const Tuple2(true, null);
  }

  Future<Tuple2<bool, String?>> signOut() async {
    try {
      await _authApi.signOut();

      // delete local
      await openBox();
      await box!.delete('appUser');
      appUser = null;
    } catch (error) {
      return Tuple2(false, error.toString());
    } finally {
      notifyListeners();
    }
    return const Tuple2(true, null);
  }

  Future<void> saveLocal() async {
    await openBox();
    if (appUser != null) {
      await box!.put('appUser', appUser!);
    } else {
      throw Exception('Nonexistent user');
    }
  }

  Future<Tuple2<bool, String?>> updateFavorite({required String address, required BuildContext context}) async {
    if (appUser == null) {
      return const Tuple2(false, 'You have to be logged in');
    }
    final bool toAdd = !appUser!.favorites.contains(address);
    try {
      if (toAdd) {
        appUser!.favorites.add(address);

      } else {
        appUser!.favorites.remove(address);
      }
      appUser = AppUser(email: appUser!.email, uid: appUser!.uid, favorites: appUser!.favorites);
      notifyListeners();
      await getFavoritesContractInfo(context: context);
      await _authApi.updateFavorite(uid: appUser!.uid, address: address, toAdd: toAdd);
      await saveLocal();
    } catch (error) {
      if (toAdd) {
        appUser!.favorites.remove(address);
      } else {
        appUser!.favorites.add(address);
      }
      notifyListeners();
      return Tuple2(false, error.toString());
    }
    notifyListeners();
    return const Tuple2(true, null);
  }

  Future<Tuple2<bool, String?>> getFavoritesContractInfo({required BuildContext context}) async {
    try {
      final List<ContractInfo> result = await context.read<SmartContractTokenProvider>().getContractsInfo(
          context: context, addresses: appUser!.favorites.map((e) => EthereumAddress.fromHex(e)).toList());
      favoriteContracts = result;
    } catch (error) {
      notifyListeners();
      return Tuple2(false, error.toString());
    }
    notifyListeners();
    return const Tuple2(true, null);
  }
}
