import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:rei_app/WalletConnectEthereumCredentials.dart';
import 'package:rei_app/models/smart_contracts/FactoryContract.g.dart';
import 'package:walletconnect_dart/walletconnect_dart.dart';
import 'package:web3dart/credentials.dart';
import 'package:web3dart/web3dart.dart';
import 'package:http/http.dart';

class FactoryContractProvider extends ChangeNotifier {
  EthereumAddress contractAddress = EthereumAddress.fromHex('0x9579d5F219Fe10aA548D8828dbBaF019A7DadA26');

  WalletConnect? connector;
  final String infuraUrl = "https://rinkeby.infura.io/v3/aef5a09436104568a84dff261b4deffc";
  Web3Client? ethClient;
  EthereumAddress? userWalletAddress;

  // final EthereumAddress contractAddress = EthereumAddress.fromHex('0x09B418394768841f094Df7d765BFd02aE2Fd2Bb2');
  DeployedContract? contract;
  WalletConnectEthereumCredentials? walletCredentials;

  Future<void> init() async {
    connector = WalletConnect(
      bridge: 'https://bridge.walletconnect.org',
      clientMeta: const PeerMeta(
        name: 'WalletConnect',
        description: 'WalletConnect Developer App',
        url: 'https://walletconnect.org',
        // icons: ['https://gblobscdn.gitbook.com/spaces%2F-LJJeCjcLrr53DcT1Ml7%2Favatar.png?alt=media'],
      ),
    );
    ethClient = Web3Client(infuraUrl, Client());
    walletCredentials =
        WalletConnectEthereumCredentials(wcProvider: EthereumWalletConnectProvider(connector!, chainId: 4));
  }


  Future<void> loadAbiCode() async {
    final String abiCode = await rootBundle.loadString('lib/app/models/smart_contracts/FactoryContract.abi.json');
    contract = DeployedContract(ContractAbi.fromJson(abiCode, 'Factory'), EthereumAddress.fromHex(contractAddress.toString()));
  }

  Future<List<EthereumAddress>> getAllContracts() async {
    final List<EthereumAddress> result = await FactoryContract(address: contractAddress, chainId: 4, client: ethClient!).getAllSecurityContracts();
    result.removeAt(0);
    return result;
  }
}
