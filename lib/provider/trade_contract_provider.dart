import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:rei_app/WalletConnectEthereumCredentials.dart';
import 'package:rei_app/models/contract/deposit_model.dart';
import 'package:rei_app/models/contract/index.dart';
import 'package:rei_app/models/smart_contracts/TradeContract.g.dart';
import 'package:rei_app/provider/smart_contract_token_provider.dart';
import 'package:tuple/tuple.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:walletconnect_dart/walletconnect_dart.dart';
import 'package:web3dart/web3dart.dart';
import 'package:http/http.dart';

class TradeContractProvider extends ChangeNotifier {
  WalletConnect? connector;
  final String infuraUrl = "https://rinkeby.infura.io/v3/aef5a09436104568a84dff261b4deffc";
  Web3Client? ethClient;
  EthereumAddress? userWalletAddress;

  EthereumAddress contractAddress = EthereumAddress.fromHex('0x93268C24424fbBc5c3bB7aE5B66344d65EF48Dc8');
  DeployedContract? contract;
  WalletConnectEthereumCredentials? walletCredentials;

  List<DepositModel>? deposits;

  List<ContractDetails>? depositsContract;

  Future<void> init() async {
    connector = WalletConnect(
      bridge: 'https://bridge.walletconnect.org',
      clientMeta: const PeerMeta(
        name: 'WalletConnect',
        description: 'WalletConnect Developer App',
        url: 'https://walletconnect.org',
        // icons: ['https://gblobscdn.gitbook.com/spaces%2F-LJJeCjcLrr53DcT1Ml7%2Favatar.png?alt=media'],
      ),
    );
    ethClient = Web3Client(infuraUrl, Client());
    walletCredentials =
        WalletConnectEthereumCredentials(wcProvider: EthereumWalletConnectProvider(connector!, chainId: 4));
  }

  Future<void> loadAbiCode() async {
    final String abiCode = await rootBundle.loadString('assets/TradeContract.json');
    contract = DeployedContract(
        ContractAbi.fromJson(abiCode, 'Trade'), EthereumAddress.fromHex('0x93268C24424fbBc5c3bB7aE5B66344d65EF48Dc8'));
  }

  Future<bool> connectWallet() async {
    SessionStatus? session;

    if (connector == null) {
      init();
    }

    session = await connector!.connect(
      chainId: 4,
      onDisplayUri: (uri) {
        launchUrl(Uri.parse(uri));
      },
    );

    userWalletAddress = EthereumAddress.fromHex(session.accounts[0]);
    notifyListeners();
    print(session);
    if (session.accounts.isEmpty) {
      throw Error();
    }

    return true;
  }

  Future<Tuple2<bool, String?>> getAllDeposits() async {
    try {
      final List<dynamic> result =
          await TradeContract(address: contractAddress, chainId: 4, client: ethClient!).getAllDeposits();
      result.removeAt(0);
      deposits = result.map((e) => DepositModel.fromArray(e)).toList();
      notifyListeners();
    } catch (error) {
      return Tuple2(false, error.toString());
    }
    print(deposits);
    return const Tuple2(true, null);
  }

  Future<Tuple2<bool, String?>> getAllDepositsContract({required BuildContext context}) async {
    try {
      final result = await getAllDeposits();
      if (!result.item1) {
        throw result;
      }
      final List<ContractDetails> tempContracts = [];
      for (final DepositModel deposit in deposits ?? []) {
        final ContractDetails result =
            await context.read<SmartContractTokenProvider>().getContractInfo(contractAddress: deposit.contractAddress);
        tempContracts.add(result);
      }
      depositsContract = tempContracts;
      notifyListeners();
    } catch (error) {
      return Tuple2(false, error.toString());
    }
    return const Tuple2(true, null);
  }



  Future<String> closeDeposit({required BigInt depositNumber, required BigInt amount})async{

    final ContractFunction contractFunction = contract!.function('closeDeposit');


    final String tx = await walletCredentials!.sendTransaction(
      Transaction(
        from: userWalletAddress,
        to: contractAddress,
        data: contractFunction.selector,
        value: EtherAmount.fromUnitAndValue(EtherUnit.wei, amount),
      ),
    );

    return tx;
  }




}
