import 'dart:typed_data';

import 'package:walletconnect_dart/walletconnect_dart.dart';
import 'package:web3dart/crypto.dart';
import 'package:web3dart/web3dart.dart';

class WalletConnectEthereumCredentials extends CustomTransactionSender {
  WalletConnectEthereumCredentials({required this.wcProvider});

  final EthereumWalletConnectProvider wcProvider;

  @override
  Future<EthereumAddress> extractAddress() async {
    return EthereumAddress.fromHex(wcProvider.connector.session.accounts[0]);
  }

  @override
  Future<String> sendTransaction(Transaction transaction) async {
    final String hash = await wcProvider.sendTransaction(
      from: transaction.from!.hex,
      to: transaction.to?.hex,
      data: transaction.data,
      gas: transaction.maxGas,
      gasPrice: transaction.gasPrice?.getInWei,
      value: transaction.value?.getInWei,
      nonce: transaction.nonce,
    );

    return hash;
  }

  @override
  Future<MsgSignature> signToSignature(
      Uint8List payload, {
        int? chainId,
        bool isEIP1559 = false,
      }) async {
    throw UnimplementedError();
  }
}